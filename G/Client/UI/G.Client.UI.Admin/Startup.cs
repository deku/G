﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(G.Client.UI.Admin.Startup))]
namespace G.Client.UI.Admin
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
