﻿using Framework.Mapping.Base;
using G.Client.Data.Entities.DeployManage;
using G.Client.Data.Wrapper;
using G.Client.Model.DeployManage.DeployServerModels;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace G.Client.Business.DeployManage
{
    public class DeployServerBusiness
    {
        public async Task<List<DeployServerViewModel>> GetList()
        {
            using (var dbContext = GDbContext.Create())
            {
                var query = from deployServer in dbContext.DeployServer.Where(ds => !ds.IsDelete)
                            join user in dbContext.User on deployServer.UpdateUserID equals user.ID
                            select new DeployServerViewModel()
                            {
                                ID = deployServer.ID,
                                Name = deployServer.Name,
                                IPAddress = deployServer.IPAddress,
                                ServerIdentity = deployServer.ServerIdentity,
                                Status = deployServer.Status,
                                UpdateUserID = deployServer.UpdateUserID,
                                UpdateUserName = user.Name,
                                UpdateTime = deployServer.UpdateTime
                            };

                return await query.ToListAsync();
            }
        }

        public async Task<int> Create(CreateDeployServerViewModel model)
        {
            using (var dbContext = GDbContext.Create())
            {
                var deployServer = new DeployServer()
                {
                    Name = model.Name,
                    ServerIdentity = model.ServerIdentity,
                    IPAddress = model.IPAddress,
                    Status = model.Status
                };
                dbContext.DeployServer.Add(deployServer);

                await dbContext.SaveChangesAsync();
                return deployServer.ID;
            }
        }

        public async Task<EditDeployServerViewModel> FindByID(int id)
        {
            using (var dbContext = GDbContext.Create())
            {
                return await dbContext.DeployServer.Select(ds => new EditDeployServerViewModel()
                {
                    ID = ds.ID,
                    IPAddress = ds.IPAddress,
                    Name = ds.Name,
                    ServerIdentity = ds.ServerIdentity,
                    Status = ds.Status
                }).SingleOrDefaultAsync(ds => ds.ID == id);
            }
        }

        public async Task<EditDeployServerViewModel> Update(EditDeployServerViewModel model)
        {
            using (var dbContext = GDbContext.Create())
            {
                var entity = await dbContext.DeployServer.SingleAsync(ds => ds.ID == model.ID);

                entity = new MapperBase<EditDeployServerViewModel, DeployServer>().GetEntity(model, entity);

                dbContext.DeployServer.Attach(entity);
                dbContext.Entry(entity).State = EntityState.Modified;
                await dbContext.SaveChangesAsync();

                return model;
            }
        }
    }
}