﻿using G.Client.Data.Entities.Accounts;
using G.Client.Data.Entities.DeployManage;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;

namespace G.Client.Data.Wrapper
{
    public class GDbContext : DbContext
    {
        static GDbContext()
        {
            //修复EF6从Nuget中添加的Bug
            var _ = typeof(System.Data.Entity.SqlServer.SqlProviderServices);

            string dbPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "DB");

            //重定向数据目录，方便测试
            AppDomain.CurrentDomain.SetData("DataDirectory", dbPath);
        }

        private GDbContext()
            : base("name=DefaultConnection")
        {
            Configuration.AutoDetectChangesEnabled = false;
            //关闭EF6.x 默认自动生成null判断语句
            Configuration.UseDatabaseNullSemantics = true;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //todo 自动注册
            modelBuilder.Entity<User>().ToTable("dbo.User");
            modelBuilder.Entity<DeployServer>().ToTable("dbo.DeployServer");
        }

        public static GDbContext Create()
        {
            return new GDbContext();
        }

        public DbSet<User> User { get; set; }

        public DbSet<DeployServer> DeployServer { get; set; }
    }
}