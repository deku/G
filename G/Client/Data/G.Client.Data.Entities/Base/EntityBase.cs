﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity;

namespace G.Client.Data.Entities.Base
{
    public class EntityBase<TPrimaryKey>
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public TPrimaryKey ID { get; set; }

        [Required]
        public bool IsDelete { get; set; } = false;

        public int CreateUserID { get; set; }

        public int UpdateUserID { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public DateTime CreateTime { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime UpdateTime { get; set; }

        public EntityBase()
        {
            var userid = HttpContext.Current?.User?.Identity?.GetUserId();
            if (userid != null)
                CreateUserID = Int32.Parse(userid);
            UpdateUserID = CreateUserID;
        }
    }
}