﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace G.Client.Infrastructure.Constant.Enums
{
    public enum DeployServerStatus
    {
        Online = 1,

        Offline = 2,
    }
}