﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace G.Client.Model.DeployManage.DeployServerModels
{
    public class EditDeployServerViewModel: CreateDeployServerViewModel
    {
        public int ID { get; set; }
    }
}