﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using G.Client.Data.Wrapper;
using G.Client.Data.Entities.Accounts;

namespace G.Client.Model.Account.IdentityModels
{
    public class UserStore : IUserStore<ApplicationUser>
    {
        public async Task CreateAsync(ApplicationUser user)
        {
            using (var dbContext = GDbContext.Create())
            {
                dbContext.User.Add(new User()
                {
                    UserName = user.UserName,
                    Password = user.Password,
                    Name = user.Name,
                    Role = user.Role,
                });

                await dbContext.SaveChangesAsync();
            }
        }

        public Task DeleteAsync(ApplicationUser user)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            //nothing to do
        }

        public Task<ApplicationUser> FindByIdAsync(string userId)
        {
            int id = 0;
            if (!Int32.TryParse(userId, out id))
            {
                throw new ArgumentException(nameof(userId));
            }

            using (var dbContext = GDbContext.Create())
            {
                var user = dbContext.User.SingleOrDefault(u => u.ID == id && !u.IsDelete);

                return Task.FromResult(new ApplicationUser()
                {
                    Id = user.ID.ToString(),
                    Name = user.Name,
                    Password = user.Password,
                    Role = user.Role,
                    UserName = user.UserName
                });
            }
        }

        public Task<ApplicationUser> FindByNameAsync(string userName)
        {
            using (var dbContext = GDbContext.Create())
            {
                var user = dbContext.User.SingleOrDefault(u => u.UserName == userName && !u.IsDelete);

                return Task.FromResult(new ApplicationUser()
                {
                    Id = user.ID.ToString(),
                    Name = user.Name,
                    Password = user.Password,
                    Role = user.Role,
                    UserName = user.UserName
                });
            }
        }

        public Task UpdateAsync(ApplicationUser user)
        {
            throw new NotImplementedException();
        }
    }
}